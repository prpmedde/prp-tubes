# README #

PRP therapy is a non-invasive therapeutic method that aims to inject platelets contained in the blood into the scalp. 
The objective is to use the essential growth factors contained in the platelets to stimulate the hair growth process.

https://prpmed.de
